import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import LoginForm from "../components/LoginForm";
import { useUser } from "../context/UserContext";
import { AppRoutes } from "../enums/routes.enum";

const Login = () => {
	const navigate = useNavigate();
	const { user } = useUser();

	const handleLogin = (username: string) => {
		// Navigate to guitars!
		console.log("Login.handleLogin", username);
	};

	useEffect(() => {
		if (user.username) {
			navigate(AppRoutes.Guitars);
		}
	}, [user.username]);

	return (
		<>
			<h1>Login to Guitars</h1>
			<LoginForm onLogin={handleLogin} />
		</>
	);
};
export default Login;
