import { useEffect, useState } from "react";
import { fetchGuitars, Guitar } from "../api/guitars";
import GuitarList from "../components/GuitarList";
import { useGuitars } from "../context/GuitarContext";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";

const Guitars = () => {
	console.log("Guitars.render()");

	const { guitars, setGuitars } = useGuitars();
	const [loading, setLoading] = useState<boolean>(true);

	const { user } = useUser();

	useEffect(() => {
		fetchGuitars().then((guitars: Guitar[]) => {
			setLoading(false);
			setGuitars(guitars);
		});
	}, []);

	return (
		<>
			<h1>Welcome {user.username} to My Guitars</h1>
			{loading && <p>Loading guitars...</p>}
			<GuitarList guitars={guitars} />
		</>
	);
};

// Use the withAuth higher order component
export default withAuth(Guitars);
