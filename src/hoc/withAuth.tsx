import React from "react";
import { Navigate } from "react-router-dom";
import { useUser } from "../context/UserContext";

const withAuth = (Component: React.FC) => (props: any) => {
	// Access the user context
	const { user } = useUser();

	// Check if the user exists and continue
	if (user.username) {
		return <Component {...props} />;
	} 
	else { // No user, redirect to Login page
		return <Navigate replace={true} to="/" />;
	}
};
export default withAuth;
