import axios from "axios";

export interface Guitar {
	id: string;
	model: string;
	manufacturer: string;
	bodyType: string;
	materials: {
		neck: string;
		fretboard: string;
		body: string;
	};
	strings: number;
	image: string;
}

export async function fetchGuitars(): Promise<Guitar[]> {
	const { data } = await axios.get<Guitar[]>(
		"https://dce-noroff-api.herokuapp.com/guitars"
	);
	return Promise.resolve(data);
}
