import GuitarProvider from "./GuitarContext";
import UserProvider from "./UserContext";

const AppContext = (props: any) => {
	return (
		<UserProvider>
			<GuitarProvider>{props.children}</GuitarProvider>
		</UserProvider>
	);
};

export default AppContext;
