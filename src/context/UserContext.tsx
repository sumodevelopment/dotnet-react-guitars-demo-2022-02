import { createContext, useContext, useState } from "react";

export interface User {
	username: string;
}

export interface UserState {
	user: User;
	login: (username: string) => void;
	logout: () => void;
}

const defaultValue: UserState = {
	user: { username: "" },
	login: (username: string) => undefined,
	logout: () => undefined,
};

// Context that keeps the state
const UserContext = createContext<UserState>(defaultValue);

export const useUser = () => {
	return useContext(UserContext);
};

interface UserProviderProps {
	children: React.ReactNode;
}
// Provider (Component), that Provides the state
const UserProvider = (props: UserProviderProps) => {
	const [user, setUser] = useState<User>({
		username: localStorage.getItem("username") || "",
	});

	function login(username: string): void {
		localStorage.setItem("username", username);
		setUser({
			username,
		});
	}

	function logout(): void {
		setUser({ username: "" });
	}

	return (
		<UserContext.Provider value={{ user, login, logout }}>
			{props.children}
		</UserContext.Provider>
	);
};

export default UserProvider;
