import { createContext, useContext, useState } from "react";
import { Guitar } from "../api/guitars";

interface GuitarState {
	guitars: Guitar[];
	guitarCount: number;
	setGuitars: (guitars: Guitar[]) => void;
}

const defaultValue: GuitarState = {
	guitars: [],
	guitarCount: 0,
	setGuitars: (guitars: Guitar[]) => undefined,
};

const GuitarContext = createContext<GuitarState>(defaultValue);

export const useGuitars = () => {
	return useContext(GuitarContext);
};

interface GuitarProviderProps {
	children: React.ReactNode;
}

export const GuitarProvider = (props: GuitarProviderProps) => {
	const [guitars, setGuitars] = useState<Guitar[]>([]);

	// Since the setGuitars update the state, the component will re-render
	// Causing this statement to run again.
	const guitarCount = guitars.length;

	return (
		<GuitarContext.Provider value={{ guitars, setGuitars, guitarCount }}>
			{props.children}
		</GuitarContext.Provider>
	);
};
export default GuitarProvider;
