import { BrowserRouter, Route, Routes } from "react-router-dom";
import Navbar from "./components/Navbar";
import { AppRoutes } from "./enums/routes.enum";
import Guitars from "./pages/Guitars";
import Login from "./pages/Login";

const App = () => {
	return (
		<BrowserRouter>
			<Navbar />
			<main>
				<Routes>
					<Route path={AppRoutes.Login} element={<Login />} />
					<Route path={AppRoutes.Guitars} element={<Guitars />} />
				</Routes>
			</main>
		</BrowserRouter>
	);
};

export default App;
