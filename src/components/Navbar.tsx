import { useGuitars } from "../context/GuitarContext";
import { useUser } from "../context/UserContext";

const Navbar = () => {
	const { user } = useUser();
	const { guitars, guitarCount } = useGuitars();

	console.log(guitars);
	

	return (
		<nav>
			<b>Guitars</b>
			{user.username && (
				<aside>
					<span>{user.username}</span>
					<span>{guitarCount}</span>
				</aside>
			)}
		</nav>
	);
};

export default Navbar;
