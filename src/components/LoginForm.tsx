import { SyntheticEvent, useState } from "react";
import { useUser } from "../context/UserContext";

interface LoginFormProps {
	onLogin: (username: string) => void;
}

const LoginForm = (props: LoginFormProps) => {
	// Local state.
	const [username, setUsername] = useState<string>("");
	const { login } = useUser();

	const onFormSubmit = (event: SyntheticEvent) => {
		event.preventDefault();
		console.log("FORM HAS BEEN SUBMITTED! 🤩", username);
		login(username);
		props.onLogin(username); // reference handleLogin
	};

	const onUsernameChange = (event: SyntheticEvent) => {
		const input = event.target as HTMLInputElement;
		setUsername(input.value.trim());
	};

	return (
		<form onSubmit={onFormSubmit}>
			<input
				type="text"
				placeholder="What's your name?"
				onChange={onUsernameChange}
			/>
			<button type="submit">Login</button>
		</form>
	);
};
export default LoginForm;
