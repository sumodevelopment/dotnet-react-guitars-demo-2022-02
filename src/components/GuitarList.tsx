import { Guitar } from "../api/guitars";

interface GuitarListProps {
	guitars: Guitar[];
}
const GuitarList = (props: GuitarListProps) => {
	
	console.log("GuitarList.render()");


	return (
		<>
			<h4>Guitars</h4>
			<ul>
				{props.guitars.map((guitar) => (
					<li key={guitar.id}>{guitar.model}</li>
				))}
			</ul>
		</>
	);
};

export default GuitarList;
